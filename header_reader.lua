-- header_reader.lua

-- Load the public dataset from a text file
function loadDataset()
    local dataset = {
        ipList = {},
        domainList = {}
    }
    local file = io.open("ip_domaine_dataset.txt", "r")
    if file then
        for line in file:lines() do
            local ip, domain = line:match("([^%s]+)%s+([^%s]+)")
            table.insert(dataset.ipList, ip)
            table.insert(dataset.domainList, domain)
        end
        file:close()
    end
    return dataset
end

-- Function to analyze the email header and detect phishing
function analyzeEmailHeader(header, dataset)
    -- Check if the sender's IP address is suspicious
    if tableContains(dataset.ipList, header.FromIP) then
        return "Suspicious sender's IP address detected. Possible phishing."
    end

    -- Check if the sender's domain is suspicious
    if tableContains(dataset.domainList, header.FromDomain) then
        return "Suspicious sender's domain detected. Possible phishing."
    end

    -- No signs of phishing detected
    return "No signs of phishing detected."
end

-- Utility function to check if a value is present in a table
function tableContains(table, value)
    for _, v in ipairs(table) do
        if v == value then
            return true
        end
    end
    return false
end

-- Read the email header arguments from the command-line interface
local header = {
    FromIP = arg[1],
    FromDomain = arg[2]
}

-- Load the public dataset
local dataset = loadDataset()

-- Call the function to analyze the email header and detect phishing
local result = analyzeEmailHeader(header, dataset)

-- Display the analysis response
print(result)
