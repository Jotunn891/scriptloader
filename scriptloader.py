import subprocess

# Ask the user to enter the email header
header = {
    "FromIP": input("Enter the sender's IP address: "),
    "FromDomain": input("Enter the sender's domain: ")
}

# Call the Lua script to analyze the email header
result = subprocess.run(["lua", "header_reader.lua", header["FromIP"], header["FromDomain"]],
                        capture_output=True, text=True)

# Display the response from the Lua script
print(result.stdout)
